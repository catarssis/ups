<?php

namespace MRW\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MRWUserBundle extends Bundle
{
	public function getParent()
    {
        return 'ApplicationSonataUserBundle';
    }
}
