<?php

namespace MRW\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CustomerType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('customerName', 'text', array('label' => 'Customer Name', 'required' => true))
            ->add('customerSurName1', 'text', array('label' => 'Customer Surname 1', 'required' => false))
            ->add('customerSurName2', 'text', array('label' => 'Customer Surname 2', 'required' => false))
            ->add('customerAdress', 'text', array('label' => 'Customer Adresse', 'required' => false))
            ->add('customerCity', 'text', array('label' => 'Customer City', 'required' => false))
            ->add('customerZip', 'text', array('label' => 'Customer Zip', 'required' => false))
            ->add('customerPhone', 'text', array('label' => 'Customer Phone', 'required' => false))
            ->add('customerMobile', 'text', array('label' => 'Customer Mobile', 'required' => false))
            ->add('customerState', 'text', array('label' => 'Customer State', 'required' => false))
            ->add('customerCountry', 'text', array('label' => 'Customer Country', 'required' => false))
            ->add('customerCode', 'text', array('label' => 'Customer Code', 'required' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MRW\SiteBundle\Entity\Customer'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mrw_sitebundle_customer';
    }
}
