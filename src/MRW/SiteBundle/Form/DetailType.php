<?php

namespace MRW\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DetailType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('recordType', 'text', array('label' => 'Record Type', 'required' => true))
            //->add('orderCode', 'text', array('label' => 'Order Code', 'required' => true))
            ->add('productId', 'text', array('label' => 'Product ID', 'required' => true, 'attr' => array( 'class' => 'productId-class' ) ))
            ->add('quantity', 'number', array('label' => 'Quantity', 'required' => true ))
            ->add('unitCost', 'number', array('label' => 'Unit Cost', 'required' => false ))
            ->add('salePrice', 'number', array('label' => 'Sale Price', 'required' => true ))
            ->add('description', 'text', array('label' => 'Description', 'required' => false ))
            //->add('warehouse', 'text', array('label' => 'Warehouse/Supplier', 'required' => false))
            //->add('header', null, array('label' => 'Header', 'required' => true))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MRW\SiteBundle\Entity\Detail'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mrw_sitebundle_detail';
    }
}
