<?php

namespace MRW\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SubStatusType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('subStatus', 'text', array('label' => 'Sub Status', 'required' => true))
            ->add('action', 'text', array('label' => 'Action', 'required' => true))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MRW\SiteBundle\Entity\SubStatus'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mrw_sitebundle_substatus';
    }
}
