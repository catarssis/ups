<?php

namespace MRW\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;


class HeaderType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('recordType', 'text', array('label' => 'Record Type', 'required' => true))
            ->add('orderType', 'choice', array('label' => 'Order Type', 'required' => true,'choices'=> array('E'=>'Envoi','R'=>'Retour', 'attr' => array('maxLength' => 1))))
            //->add('orderCode', 'text', array('label' => 'Order Code', 'required' => true))


            ->add('trackingId', 'text', array('label' => 'Numero Tracking', 'required' => true, 'attr' => array('maxLength' => 12)))

            ->add('customerCode', 'text', array('label' => 'Customer Code', 'required' => true, 'attr' => array('maxLength' => 20)))
            //->add('serviceDate', 'date', array('label' => 'Service Date', 'required' => true))
            //->add('transmitionDate', 'date', array('label' => 'Transmission Date', 'required' => false))
            //->add('preparationCenter', 'text', array('label' => 'Preparation Center', 'required' => true))
            ->add('customerName_recepteur', 'text', array('label' => 'Customer Name', 'required' => true))
            ->add('customerSurName1_recepteur', 'text', array('label' => 'Customer Surname 1', 'required' => false))
            ->add('customerSurName2_recepteur', 'text', array('label' => 'Customer Surname 2', 'required' => false))
            ->add('customerAdress_recepteur', 'text', array('label' => 'Customer Adresse', 'required' => true))
            ->add('customerCity_recepteur', 'text', array('label' => 'Customer City', 'required' => true))
            ->add('customerZip_recepteur', 'text', array('label' => 'Customer Zip', 'required' => true))
            ->add('customerPhone_recepteur', 'text', array('label' => 'Customer Phone', 'required' => true))
            ->add('customerMobile_recepteur', 'text', array('label' => 'Customer Mobile', 'required' => false))
            ->add('customerState_recepteur', 'text', array('label' => 'Customer State', 'required' => true))
            ->add('customerCountry_recepteur', 'choice', array('label' => 'Customer Country', 'required' => true,'choices'=> array('ES'=>'ES','PT'=>'PT','AD'=>'AD')))
            ->add('vatId', 'text', array('label' => 'VAT ID', 'required' => false))
            ->add('weight', 'number', array('label' => 'Weight', 'required' => true))
            ->add('volume', 'number', array('label' => 'Volume', 'required' => false))
            //->add('bundles', 'integer', array('label' => 'Bundles', 'required' => false))
            //->add('refund', 'choice', array('label' => 'refund', 'required' => false,'choices'=> array('N'=>'Non','O'=>'Oui')))
            //->add('refundValue', 'number', array('label' => 'Refund Value', 'required' => false))
            ->add('comments1', 'text', array('label' => 'Comments 1', 'required' => false))
            ->add('trackingId', 'text', array('label' => 'Tracking ID', 'required' => true, 'mapped' => false, 'attr' => array('class' => 'trackingId-search')))
            ->add('deliveryType', 'choice', array('label' => 'Delivery Type', 'required' => true,'choices'=> array('D'=>'Point of delivery','A'=>'Agency')))
            ->add('comments2', 'text', array('label' => 'Comments 2', 'required' => false))
            ->add('email', 'email', array('label' => 'Email', 'required' => false))
            ->add('sms', 'text', array('label' => 'SMS Identité', 'required' => true))
            ->add('echange', 'choice', array('label' => 'Echange', 'required' => false,'choices'=> array('S'=>'S','N'=>'N')))
            ->add('transporteur', 'choice', array('label' => 'Transporteur', 'required' => true,'choices'=> array('MRW'=>'MRW','TTM'=>'TTM','ADER'=>'ADER')))
            ->add('typeService', 'choice', array('label' => 'Type Service', 'required' => true,'choices'=> array(
                    'URGENTE_10:00'=>'URGENTE_10:00',
                    'URGENTE_12:00'=>'URGENTE_12:00',
                    // 'URGENTE_14:00'=>'URGENTE_14:00',
                    'URGENTE_19:00'=>'URGENTE_19:00',
                    'ECOMMERCE'=>'ECOMMERCE',
                    // 'SERVICIO URGENTE 8:30H EXPEDICIÓN'=>'SERVICIO URGENTE 8:30H EXPEDICIÓN',
                    // 'URGENTE_10EXP'=>'URGENTE_10EXP',
                    // 'URGENTE_12EXP'=>'URGENTE_12EXP',
                    // 'URGENTE_14EXP'=>'URGENTE_14EXP',
                    // 'URGENTE_19EXP'=>'URGENTE_19EXP',
                    'URGENTE_8:30'=>'URGENTE_8:30',
                    'MARITIMO_BALEARES'=>'MARITIMO_BALEARES'))
            )
            //->add('status', 'text', array('label' => 'Status', 'required' => false))
            //->add('subStatus', 'text', array('label' => 'Substatus', 'required' => false))
            ->add('service', 'choice', array('label' => 'Service', 'required' => false,'choices'=> array('Pre 10'=>'Pre 10','Pre 12'=>'Pre 12','Pre 19'=>'Pre 19','Eco+'=>'Eco+')))
            ->add('packaging', 'choice', array('label' => 'Packaging', 'required' => false,'choices'=> array('Env'=>'Env','Pack'=>'Pack','Box'=>'Box','Tube'=>'Tube','Other'=>'Other')))
            //->add('statusDate', 'date', array('label' => 'Status Date', 'required' => false))
            //->add('issue', 'text', array('label' => 'Issue', 'required' => false))
            //->add('comments', 'text', array('label' => 'Comments', 'required' => false))
            //->add('actif', null, array('label' => 'Actif', 'required' => false))
            ->add('canceled', null, array('label' => 'Canceled', 'required' => false))
            //->add('generated', null, array('label' => 'Generated', 'required' => false))
            ->add('customer', null, array('label' => 'Expediteur', 'required' => false))
            ->add('codeBarre', null, array('label' => 'CodeBarre', 'required' => false))
            ->add('campaniaMRW', null, array('label' => 'Campania MRW', 'required' => false))
            ->add('entregaSabado', 'choice', array('label' => 'Samedi', 'required' => false,'choices'=> array('N' => 'N', 'S' => 'S')))
            ->add('details', CollectionType::class, array('entry_type' => DetailType::class, 'allow_add' => true, 'by_reference' => false))
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MRW\SiteBundle\Entity\Header'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mrw_sitebundle_header';
    }
}
