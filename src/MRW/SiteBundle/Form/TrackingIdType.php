<?php

namespace MRW\SiteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TrackingIdType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('trackingid', 'text', array('label' => 'Tracking ID', 'required' => true))
            // ->add('used', 'text', array('label' => 'Used', 'required' => true))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MRW\SiteBundle\Entity\TrackingId'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mrw_sitebundle_trackingid';
    }
}
