<?php

namespace MRW\SiteBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MRWSiteBundle extends Bundle
{
	public function getParent()
    {
        return 'SonataDoctrineORMAdminBundle';
    }
}
