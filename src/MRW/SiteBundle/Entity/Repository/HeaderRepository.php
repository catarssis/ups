<?php

namespace MRW\SiteBundle\Entity\Repository;

/**
 * HeaderRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class HeaderRepository extends \Doctrine\ORM\EntityRepository
{

	public function getCanceledHeaders()
	{

		$aHeaders = $this->_em->createQuery('SELECT	h
			FROM MRWSiteBundle:Header h
			WHERE h.canceled = :canceled AND h.generated = :generated AND h.generatedCanceled = :generatedCanceled'
			)
			->setParameters(array('canceled' => true, 'generated' => true, 'generatedCanceled' => false))
			->getResult();

			return $aHeaders;
	}

	public function getHeaders()
	{

		$aHeaders = $this->_em->createQuery('SELECT	h, d
			FROM MRWSiteBundle:Header h
			JOIN h.details d
			WHERE h.canceled = :canceled AND h.generated = :generated AND h.generatedCanceled = :generatedCanceled'
			)
			->setParameters(array('canceled' => false, 'generated' => false, 'generatedCanceled' => false))
			->getResult();

			return $aHeaders;
	}

	public function getHeaderByTrackingId($trackingId)
	{
		return $this->_em->createQuery('SELECT	h
			FROM MRWSiteBundle:Header h
			JOIN h.trackingId ti
			WHERE ti.trackingId = :trackingId'
			)
			->setParameters(array('trackingId' => $trackingId))
			->getOneorNullResult();	
	}		
}
