<?php

namespace MRW\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MRW\SiteBundle\Entity\Repository\RoutaRepository")
 * @ORM\Table(name="routa")
 */
class Routa
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**    
    * @ORM\Column(name="codePays", type="string",length=255, nullable=true) 
    */
    protected $codePays;

    /**    
    * @ORM\Column(name="codePostal", type="string",length=255, nullable=true) 
    */
    protected $codePostal;

    /**    
    * @ORM\Column(name="codeAgence", type="string",length=255, nullable=true) 
    */
    protected $codeAgence;    
    

    /**    
    * @ORM\Column(name="nomAgence", type="string",length=255, nullable=true) 
    */
    protected $nomAgence; 

    /**    
    * @ORM\Column(name="nomRoute", type="string",length=255, nullable=true) 
    */
    protected $nomRoute;  
    
    /**    
    * @ORM\Column(name="codeRoute", type="string",length=255, nullable=true) 
    */
    protected $codeRoute;         
   

    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codePays
     *
     * @param string $codePays
     *
     * @return Routa
     */
    public function setCodePays($codePays)
    {
        $this->codePays = $codePays;

        return $this;
    }

    /**
     * Get codePays
     *
     * @return string
     */
    public function getCodePays()
    {
        return $this->codePays;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return Routa
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set codeAgence
     *
     * @param string $codeAgence
     *
     * @return Routa
     */
    public function setCodeAgence($codeAgence)
    {
        $this->codeAgence = $codeAgence;

        return $this;
    }

    /**
     * Get codeAgence
     *
     * @return string
     */
    public function getCodeAgence()
    {
        return $this->codeAgence;
    }


    /**
     * Set nomAgence
     *
     * @param string $nomAgence
     *
     * @return Routa
     */
    public function setNomAgence($nomAgence)
    {
        $this->nomAgence = $nomAgence;

        return $this;
    }

    /**
     * Get nomAgence
     *
     * @return string
     */
    public function getNomAgence()
    {
        return $this->nomAgence;
    }

    /**
     * Set nomRoute
     *
     * @param string $nomRoute
     *
     * @return Routa
     */
    public function setNomRoute($nomRoute)
    {
        $this->nomRoute = $nomRoute;

        return $this;
    }

    /**
     * Get nomRoute
     *
     * @return string
     */
    public function getNomRoute()
    {
        return $this->nomRoute;
    }

    /**
     * Set codeRoute
     *
     * @param string $codeRoute
     *
     * @return Routa
     */
    public function setCodeRoute($codeRoute)
    {
        $this->codeRoute = $codeRoute;

        return $this;
    }

    /**
     * Get codeRoute
     *
     * @return string
     */
    public function getCodeRoute()
    {
        return $this->codeRoute;
    }
}
