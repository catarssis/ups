<?php

namespace MRW\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MRW\SiteBundle\Entity\Repository\DetailRepository")
 * @ORM\Table(name="detail")
 * @ORM\HasLifecycleCallbacks()
 */
class Detail
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**    
    * @ORM\Column(name="description", type="string", length=255, nullable=true) 
    */
    protected $description;    

    /**    
    * @ORM\Column(name="recordType", type="string", length=255, nullable=true) 
    */
    protected $recordType ='L';

    /**    
    * @ORM\Column(name="orderCode", type="string", length=255, nullable=true) 
    */
    protected $orderCode;

    /**    
    * @ORM\Column(name="productId", type="string", length=255, nullable=true) 
    */
    protected $productId = '00917_1970BULTOPAXD';

    /**    
    * @ORM\Column(name="quantity", type="integer", nullable=true) 
    */
    protected $quantity;

    /**    
    * @ORM\Column(name="unitCost", type="integer", nullable=true) 
    */
    protected $unitCost;

    /**    
    * @ORM\Column(name="salePrice", type="integer", nullable=true) 
    */
    protected $salePrice;

    /**    
    * @ORM\Column(name="warehouse", type="string", length=255, nullable=true) 
    */
    protected $warehouse;

    /**    
    * @ORM\Column(name="customerCode", type="string", length=255, nullable=true) 
    */
    protected $customerCode;

    /** @ORM\ManyToOne(targetEntity="Header", inversedBy="details") */
    protected $header;

    /** @ORM\Column(name="actif", type="boolean") */
    protected $actif;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->actif = true;
    }


    public function __toString()
    {
        return $this->id;
    }


    public function getAllProperties()
    {

        return get_object_vars($this);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recordType
     *
     * @param string $recordType
     *
     * @return Detail
     */
    public function setRecordType($recordType)
    {
        //$this->recordType = $recordType;
        $this->recordType = $this->header->getRecordType();
        return $this;
    }

    /**
     * Get recordType
     *
     * @return string
     */
    public function getRecordType()
    {
        return $this->recordType;
    }

    /**
     * Set orderCode
     *
     * @param string $orderCode
     *
     * @return Detail
     */
    public function setOrderCode($orderCode)
    {
        // $this->orderCode = $orderCode;
        $this->orderCode = $this->getHeader()->getOrderCode();

        return $this;
    }

    /**
     * Get orderCode
     *
     * @return string
     */
    public function getOrderCode()
    {
        return $this->orderCode;
    }

    /**
     * Set productId
     *
     * @param string $productId
     *
     * @return Detail
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return string
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return Detail
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set unitCost
     *
     * @param integer $unitCost
     *
     * @return Detail
     */
    public function setUnitCost($unitCost)
    {
        $this->unitCost = $unitCost;

        return $this;
    }

    /**
     * Get unitCost
     *
     * @return integer
     */
    public function getUnitCost()
    {
        return $this->unitCost;
    }

    /**
     * Set salePrice
     *
     * @param integer $salePrice
     *
     * @return Detail
     */
    public function setSalePrice($salePrice)
    {
        $this->salePrice = $salePrice;

        return $this;
    }

    /**
     * Get salePrice
     *
     * @return integer
     */
    public function getSalePrice()
    {
        return $this->salePrice;
    }

    /**
     * Set customerCode
     *
     * @param string $customerCode
     *
     * @return Detail
     */
    public function setCustomerCode($customerCode)
    {
        // ladybug_dump('SET', $this->getHeader()->getCustomerCode());
        // die();
        $this->customerCode = $this->getHeader()->getCustomerCode();

        return $this;
    }

    /**
    * @ORM\PrePersist
    */
    public function setPostCustomerCode()
    {
        
        $this->setCustomerCode($this->getHeader()->getCustomerCode());
        $this->setOrderCode($this->getHeader()->getOrderCode());
       
        return $this;
    }

    /**
     * Get customerCode
     *
     * @return string
     */
    public function getCustomerCode()
    {
        return $this->customerCode;
    }

    /**
     * Set warehouse
     *
     * @param string $warehouse
     *
     * @return Detail
     */
    public function setWarehouse($warehouse)
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    /**
     * Get warehouse
     *
     * @return string
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * Set header
     *
     * @param \MRW\SiteBundle\Entity\Header $header
     *
     * @return Detail
     */
    public function setHeader(\MRW\SiteBundle\Entity\Header $header = null)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * Get header
     *
     * @return \MRW\SiteBundle\Entity\Header
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set actif
     *
     * @param boolean $actif
     *
     * @return Detail
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return boolean
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Detail
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
