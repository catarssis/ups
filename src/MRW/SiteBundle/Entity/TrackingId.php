<?php

namespace MRW\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MRW\SiteBundle\Entity\Repository\TrackingIdRepository")
 * @ORM\Table(name="trackingid")
 */
class TrackingId
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @ORM\Column(name="trackingID", type="string", length=22, nullable=true) 
    */
    protected $trackingId;


    /** @ORM\Column(name="used", type="boolean") */
    protected $used = false;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->used = false;
    }
     

    public function __toString()
    {
        return $this->trackingId;
    }


   

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set trackingId
     *
     * @param string $trackingId
     *
     * @return TrackingId
     */
    public function setTrackingId($trackingId)
    {
        $this->trackingId = $trackingId;

        return $this;
    }

    /**
     * Get trackingId
     *
     * @return string
     */
    public function getTrackingId()
    {
        return $this->trackingId;
    }

    /**
     * Set used
     *
     * @param boolean $used
     *
     * @return TrackingId
     */
    public function setUsed($used)
    {
        $this->used = $used;

        return $this;
    }

    /**
     * Get used
     *
     * @return boolean
     */
    public function getUsed()
    {
        return $this->used;
    }
}
