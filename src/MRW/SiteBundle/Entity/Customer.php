<?php

namespace MRW\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MRW\SiteBundle\Entity\Repository\CustomerRepository")
 * @ORM\Table(name="customer")
 */
class Customer
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**    
    * @ORM\Column(name="customerName", type="string", length=255, nullable=true) 
    */
    protected $customerName;

    /**    
    * @ORM\Column(name="customerSurName1", type="string", length=255, nullable=true) 
    */
    protected $customerSurName1;

    /**    
    * @ORM\Column(name="customerSurName2", type="string", length=255, nullable=true) 
    */
    protected $customerSurName2;

    /**    
    * @ORM\Column(name="customerAdress", type="string", length=255, nullable=true) 
    */
    protected $customerAdress;

    /**    
    * @ORM\Column(name="customerCity", type="string", length=255, nullable=true) 
    */
    protected $customerCity;

    /**    
    * @ORM\Column(name="customerZip", type="string", length=255, nullable=true) 
    */
    protected $customerZip;

    /**    
    * @ORM\Column(name="customerPhone", type="string", length=255, nullable=true) 
    */
    protected $customerPhone;

    /**    
    * @ORM\Column(name="customerMobile", type="string", length=255, nullable=true) 
    */
    protected $customerMobile;

    /**    
    * @ORM\Column(name="customerState", type="string", length=255, nullable=true) 
    */
    protected $customerState;

    /**    
    * @ORM\Column(name="customerCountry", type="string", length=255, nullable=true) 
    */
    protected $customerCountry;

    /**    
    * @ORM\Column(name="customerCode", type="string", length=255, nullable=true) 
    */
    protected $customerCode;

    /**
     * @ORM\OneToMany(targetEntity="Header", mappedBy="customer", cascade="all")
     */
    protected $headers;

    /** @ORM\Column(name="actif", type="boolean") */
    protected $actif;


    public function __toString()
    {
        return $this->customerName;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->headers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->actif = true;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customerName
     *
     * @param string $customerName
     *
     * @return Customer
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;

        return $this;
    }

    /**
     * Get customerName
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * Set customerSurName1
     *
     * @param string $customerSurName1
     *
     * @return Customer
     */
    public function setCustomerSurName1($customerSurName1)
    {
        $this->customerSurName1 = $customerSurName1;

        return $this;
    }

    /**
     * Get customerSurName1
     *
     * @return string
     */
    public function getCustomerSurName1()
    {
        return $this->customerSurName1;
    }

    /**
     * Set customerSurName2
     *
     * @param string $customerSurName2
     *
     * @return Customer
     */
    public function setCustomerSurName2($customerSurName2)
    {
        $this->customerSurName2 = $customerSurName2;

        return $this;
    }

    /**
     * Get customerSurName2
     *
     * @return string
     */
    public function getCustomerSurName2()
    {
        return $this->customerSurName2;
    }

    /**
     * Set customerAdress
     *
     * @param string $customerAdress
     *
     * @return Customer
     */
    public function setCustomerAdress($customerAdress)
    {
        $this->customerAdress = $customerAdress;

        return $this;
    }

    /**
     * Get customerAdress
     *
     * @return string
     */
    public function getCustomerAdress()
    {
        return $this->customerAdress;
    }

    /**
     * Set customerCity
     *
     * @param string $customerCity
     *
     * @return Customer
     */
    public function setCustomerCity($customerCity)
    {
        $this->customerCity = $customerCity;

        return $this;
    }

    /**
     * Get customerCity
     *
     * @return string
     */
    public function getCustomerCity()
    {
        return $this->customerCity;
    }

    /**
     * Set customerZip
     *
     * @param string $customerZip
     *
     * @return Customer
     */
    public function setCustomerZip($customerZip)
    {
        $this->customerZip = $customerZip;

        return $this;
    }

    /**
     * Get customerZip
     *
     * @return string
     */
    public function getCustomerZip()
    {
        return $this->customerZip;
    }

    /**
     * Set customerPhone
     *
     * @param string $customerPhone
     *
     * @return Customer
     */
    public function setCustomerPhone($customerPhone)
    {
        $this->customerPhone = $customerPhone;

        return $this;
    }

    /**
     * Get customerPhone
     *
     * @return string
     */
    public function getCustomerPhone()
    {
        return $this->customerPhone;
    }

    /**
     * Set customerMobile
     *
     * @param string $customerMobile
     *
     * @return Customer
     */
    public function setCustomerMobile($customerMobile)
    {
        $this->customerMobile = $customerMobile;

        return $this;
    }

    /**
     * Get customerMobile
     *
     * @return string
     */
    public function getCustomerMobile()
    {
        return $this->customerMobile;
    }

    /**
     * Set customerState
     *
     * @param string $customerState
     *
     * @return Customer
     */
    public function setCustomerState($customerState)
    {
        $this->customerState = $customerState;

        return $this;
    }

    /**
     * Get customerState
     *
     * @return string
     */
    public function getCustomerState()
    {
        return $this->customerState;
    }

    /**
     * Set customerCountry
     *
     * @param string $customerCountry
     *
     * @return Customer
     */
    public function setCustomerCountry($customerCountry)
    {
        $this->customerCountry = $customerCountry;

        return $this;
    }

    /**
     * Get customerCountry
     *
     * @return string
     */
    public function getCustomerCountry()
    {
        return $this->customerCountry;
    }

    /**
     * Set customerCode
     *
     * @param string $customerCode
     *
     * @return Customer
     */
    public function setCustomerCode($customerCode)
    {
        $this->customerCode = $customerCode;

        return $this;
    }

    /**
     * Get customerCode
     *
     * @return string
     */
    public function getCustomerCode()
    {
        return $this->customerCode;
    }

    /**
     * Add header
     *
     * @param \MRW\SiteBundle\Entity\Header $header
     *
     * @return Customer
     */
    public function addHeader(\MRW\SiteBundle\Entity\Header $header)
    {
        $this->headers[] = $header;

        return $this;
    }

    /**
     * Remove header
     *
     * @param \MRW\SiteBundle\Entity\Header $header
     */
    public function removeHeader(\MRW\SiteBundle\Entity\Header $header)
    {
        $this->headers->removeElement($header);
    }

    /**
     * Get headers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * Set actif
     *
     * @param boolean $actif
     *
     * @return Customer
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return boolean
     */
    public function getActif()
    {
        return $this->actif;
    }
}
