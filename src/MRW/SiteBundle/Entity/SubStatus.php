<?php

namespace MRW\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MRW\SiteBundle\Entity\Repository\SubStatusRepository")
 * @ORM\Table(name="substatus")
 */
class SubStatus
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**    
    * @ORM\Column(name="subStatus", type="string", length=255, nullable=true) 
    */
    protected $subStatus;

    /**    
    * @ORM\Column(name="action", type="string", length=255, nullable=true) 
    */
    protected $action;


    public function __toString()
    {
        return $this->id;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subStatus
     *
     * @param string $subStatus
     *
     * @return SubStatus
     */
    public function setSubStatus($subStatus)
    {
        $this->subStatus = $subStatus;

        return $this;
    }

    /**
     * Get subStatus
     *
     * @return string
     */
    public function getSubStatus()
    {
        return $this->subStatus;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return SubStatus
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

}
