<?php

namespace MRW\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MRW\SiteBundle\Entity\Repository\HeaderRepository")
 * @ORM\Table(name="header")
 */
class Header
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**    
    * @ORM\Column(name="recordType", type="string", length=1, nullable=true) 
    */
    protected $recordType = 'H';

    /**    
    * @ORM\Column(name="orderType", type="string", length=1, nullable=true) 
    */
    protected $orderType; // liste déroulante E ou R    

    /**    
    * @ORM\Column(name="orderCode", type="string", length=20, nullable=true) 
    */
    protected $orderCode;

    /**    
    * @ORM\Column(name="customerCode", type="string", length=20, nullable=true) 
    */
    protected $customerCode = '00971';

    /**    
    * @ORM\Column(name="serviceDate", type="date", nullable=true) 
    */
    protected $serviceDate;

    
    /**    
    * @ORM\Column(name="transmitionDate", type="date", nullable=true) 
    */
    protected $transmitionDate; // Date de génération du fichier ddmmyyyy
 
    /**    
    * @ORM\Column(name="preparationCenter", type="string", length=20, nullable=true) 
    */
    protected $preparationCenter = '01';

      /**    
    * @ORM\Column(name="customerName", type="string", length=30, nullable=true) 
    */
    protected $customerName_recepteur;

    /**    
    * @ORM\Column(name="customerSurName1", type="string", length=30, nullable=true) 
    */
    protected $customerSurName1_recepteur;

    /**    
    * @ORM\Column(name="customerSurName2", type="string", length=30, nullable=true) 
    */
    protected $customerSurName2_recepteur;

    /**    
    * @ORM\Column(name="customerAdress", type="string", length=80, nullable=true) 
    */
    protected $customerAdress_recepteur;

    /**    
    * @ORM\Column(name="customerCity", type="string", length=20, nullable=true) 
    */
    protected $customerCity_recepteur;

    /**    
    * @ORM\Column(name="customerZip", type="string", length=20, nullable=true) 
    */
    protected $customerZip_recepteur;

    /**    
    * @ORM\Column(name="customerPhone", type="string", length=20, nullable=true) 
    */
    protected $customerPhone_recepteur;

    /**    
    * @ORM\Column(name="customerMobile", type="string", length=20, nullable=true) 
    */
    protected $customerMobile_recepteur;

    /**    
    * @ORM\Column(name="customerState", type="string", length=20, nullable=true) 
    */
    protected $customerState_recepteur;

    /**    
    * @ORM\Column(name="customerCountry", type="string", length=2, nullable=true) 
    */
    protected $customerCountry_recepteur;

    /**    
    * @ORM\Column(name="vatId", type="string", length=9, nullable=true) 
    */
    protected $vatId;

    /**    
    * @ORM\Column(name="weight", type="float", nullable=true) 
    */
    protected $weight;

    /**    
    * @ORM\Column(name="volume", type="float", nullable=true) 
    */
    protected $volume;

    /**    
    * @ORM\Column(name="bundles", type="integer", nullable=true) 
    */
    protected $bundles = 1; // nombre de lignes (detail)

    /**    
    * @ORM\Column(name="refund", type="string", length=1, nullable=true) 
    */
    protected $refund = 'N';

    /**    
    * @ORM\Column(name="refundValue", type="float", nullable=true) 
    */
    protected $refundValue = 0.00;

    /**    
    * @ORM\Column(name="comments1", type="string", length=50, nullable=true) 
    */
    protected $comments1;

    /**
    * @ORM\OneToOne(targetEntity="TrackingId", cascade="all")
    */
     protected $trackingId;

    /**    
    * @ORM\Column(name="deliveryType", type="string", length=1, nullable=true) 
    */
    protected $deliveryType = 'D'; // liste déroulante A/D

    /**    
    * @ORM\Column(name="comments2", type="string", length=50, nullable=true) 
    */
    protected $comments2;

    /**    
    * @ORM\Column(name="email", type="string", length=50, nullable=true) 
    */
    protected $email;

    /**    
    * @ORM\Column(name="SMS", type="string", length=20, nullable=true) 
    */
    protected $SMS;
    
    /**    
    * @ORM\Column(name="Echange", type="string", length=1, nullable=true) 
    */
    protected $Echange = 'N';  //oui ou non

    /**    
    * @ORM\Column(name="Transporteur", type="string", length=10, nullable=true) 
    */
    protected $Transporteur; // MRW, TTM or ADER

    /**    
    * @ORM\Column(name="TypeService", type="string", length=20, nullable=true) 
    */
    protected $TypeService;

    /**    
    * @ORM\Column(name="service", type="string", length=255, nullable=true) 
    */
    protected $service; // liste déroulante  Pre 10, Pre 12, Pre 19, ECO+


    /**    
    * @ORM\Column(name="packaging", type="string", length=255, nullable=true) 
    */
    protected $packaging; // liste déroulante  Env, Pack, Box, Tube, Other


    /**    
    * @ORM\Column(name="issue", type="string", length=20, nullable=true) 
    */
    protected $issue;

    /**    
    * @ORM\Column(name="comments", type="string", length=30, nullable=true) 
    */
    protected $comments;

    
    /**
     * @ORM\OneToMany(targetEntity="Detail", mappedBy="header", cascade="all")
     */
    protected $details;

    /** @ORM\ManyToOne(targetEntity="Customer", inversedBy="headers") */
    protected $customer;

    /** @ORM\Column(name="actif", type="boolean", nullable=true) */
    protected $actif = true;

  
    /**    
    * @ORM\Column(name="status", type="string", length=5, nullable=true) 
    */
    protected $status;

    /**    
    * @ORM\Column(name="subStatus", type="string", length=5, nullable=true) 
    */
    protected $subStatus;

    /**    
    * @ORM\Column(name="subStatusText", type="string", length=225, nullable=true) 
    */
    protected $subStatusText;    

    /**    
    * @ORM\Column(name="codeBarre", type="string", length=255, nullable=true) 
    */
    protected $codeBarre;    
    
    /** @ORM\Column(name="canceled", type="boolean") */
    protected $canceled = false;

    /** @ORM\Column(name="file_generated", type="boolean", nullable=true) */
    protected $generated = false; 

    /** @ORM\Column(name="generatedCanceled", type="boolean", nullable=true) */
    protected $generatedCanceled = false;           

     /**    
    * @ORM\Column(name="statusDate", type="date", nullable=true) 
    */
    protected $statusDate;

    

    // *    
    // * @ORM\Column(name="trackingID", type="string", length=12, nullable=true) 
    
    // protected $trackingId;    



    public function __toString()
    {
        return $this->getOrderCode();
    }


    public function getAllProperties()
    {

        return get_object_vars($this);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recordType
     *
     * @param string $recordType
     *
     * @return Header
     */
    public function setRecordType($recordType)
    {
        $this->recordType = $recordType;

        return $this;
    }

    /**
     * Get recordType
     *
     * @return string
     */
    public function getRecordType()
    {
        return $this->recordType;
    }
 
    /**
     * Set orderType
     *
     * @param string $orderType
     *
     * @return Header
     */
    public function setOrderType($orderType)
    {
        $this->orderType = $orderType;

        return $this;
    }

    /**
     * Get orderType
     *
     * @return string
     */
    public function getOrderType()
    {
        return $this->orderType;
    }

    /**
     * Set orderCode
     *
     * @param string $orderCode
     *
     * @return Header
     */
    public function setOrderCode($orderCode)
    {
        $this->orderCode = $orderCode;

        return $this;
    }

    /**
     * Get orderCode
     *
     * @return string
     */
    public function getOrderCode()
    {
        return $this->orderCode;
    }

    /**
     * Set serviceDate
     *
     * @param \Date $serviceDate
     *
     * @return Header
     */
    public function setServiceDate(\DateTime $serviceDate)
    {
        $this->serviceDate = $serviceDate;

        return $this;
    }

    /**
     * Get serviceDate
     *
     * @return \Date
     */
    public function getServiceDate()
    {
        return $this->serviceDate;
    }

    /**
     * Set transmitionDate
     *
     * @param \Date $transmitionDate
     *
     * @return Header
     */
    public function setTransmitionDate(\DateTime $transmitionDate)
    {
        $this->transmitionDate = $transmitionDate;

        return $this;
    }

    /**
     * Get transmitionDate
     *
     * @return \Date
     */
    public function getTransmitionDate()
    {
        return $this->transmitionDate;
    }

    /**
     * Set preparationCenter
     *
     * @param string $preparationCenter
     *
     * @return Header
     */
    public function setPreparationCenter($preparationCenter)
    {
        $this->preparationCenter = $preparationCenter;

        return $this;
    }

    /**
     * Get preparationCenter
     *
     * @return string
     */
    public function getPreparationCenter()
    {
        return $this->preparationCenter;
    }

   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->details = new \Doctrine\Common\Collections\ArrayCollection();
        $this->serviceDate = new \DateTime();
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Header
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set subStatus
     *
     * @param string $subStatus
     *
     * @return Header
     */
    public function setSubStatus($subStatus)
    {
        $this->subStatus = $subStatus;

        return $this;
    }

    /**
     * Get subStatus
     *
     * @return string
     */
    public function getSubStatus()
    {
        return $this->subStatus;
    }

    /**
     * Set service
     *
     * @param string $service
     *
     * @return Header
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set packaging
     *
     * @param string $packaging
     *
     * @return Header
     */
    public function setPackaging($packaging)
    {
        $this->packaging = $packaging;

        return $this;
    }

    /**
     * Get packaging
     *
     * @return string
     */
    public function getPackaging()
    {
        return $this->packaging;
    }

    /**
     * Set statusDate
     *
     * @param \DateTime $statusDate
     *
     * @return Header
     */
    public function setStatusDate($statusDate)
    {
        $this->statusDate = $statusDate;

        return $this;
    }

    /**
     * Get statusDate
     *
     * @return \DateTime
     */
    public function getStatusDate()
    {
        return $this->statusDate;
    }

    /**
     * Set vatId
     *
     * @param string $vatId
     *
     * @return Header
     */
    public function setVatId($vatId)
    {
        $this->vatId = $vatId;

        return $this;
    }

    /**
     * Get vatId
     *
     * @return string
     */
    public function getVatId()
    {
        return $this->vatId;
    }

    /**
     * Set weight
     *
     * @param float $weight
     *
     * @return Header
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set volume
     *
     * @param float $volume
     *
     * @return Header
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume
     *
     * @return float
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set bundles
     *
     * @param integer $bundles
     *
     * @return Header
     */
    public function setBundles($bundles)
    {
        $this->bundles = $bundles;

        return $this;
    }

    /**
     * Get bundles
     *
     * @return integer
     */
    public function getBundles()
    {
        return $this->bundles;
    }

    /**
     * Set refund
     *
     * @param string $refund
     *
     * @return Header
     */
    public function setRefund($refund)
    {
        $this->refund = $refund;

        return $this;
    }

    /**
     * Get refund
     *
     * @return string
     */
    public function getRefund()
    {
        return $this->refund;
    }

    /**
     * Set refundValue
     *
     * @param float $refundValue
     *
     * @return Header
     */
    public function setRefundValue($refundValue)
    {
        $this->refundValue = $refundValue;

        return $this;
    }

    /**
     * Get refundValue
     *
     * @return float
     */
    public function getRefundValue()
    {
        return $this->refundValue;
    }

    /**
     * Set comments1
     *
     * @param string $comments1
     *
     * @return Header
     */
    public function setComments1($comments1)
    {
        $this->comments1 = $comments1;

        return $this;
    }

    /**
     * Get comments1
     *
     * @return string
     */
    public function getComments1()
    {
        return $this->comments1;
    }

    /**
     * Set deliveryType
     *
     * @param string $deliveryType
     *
     * @return Header
     */
    public function setDeliveryType($deliveryType)
    {
        $this->deliveryType = $deliveryType;

        return $this;
    }

    /**
     * Get deliveryType
     *
     * @return string
     */
    public function getDeliveryType()
    {
        return $this->deliveryType;
    }

    /**
     * Set comments2
     *
     * @param string $comments2
     *
     * @return Header
     */
    public function setComments2($comments2)
    {
        $this->comments2 = $comments2;

        return $this;
    }

    /**
     * Get comments2
     *
     * @return string
     */
    public function getComments2()
    {
        return $this->comments2;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Header
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set SMS
     *
     * @param string $SMS
     *
     * @return Header
     */
    public function setSMS($SMS)
    {
        $this->SMS = $SMS;

        return $this;
    }

    /**
     * Get SMS
     *
     * @return string
     */
    public function getSMS()
    {
        return $this->SMS;
    }


    /**
     * Set Echange
     *
     * @param string $Echange
     *
     * @return Header
     */
    public function setEchange($Echange)
    {
        $this->Echange = $Echange;

        return $this;
    }

    /**
     * Get Echange
     *
     * @return string
     */
    public function getEchange()
    {
        return $this->Echange;
    }

    /**
     * Set Transporteur
     *
     * @param string $Transporteur
     *
     * @return Header
     */
    public function setTransporteur($Transporteur)
    {
        $this->Transporteur = $Transporteur;

        return $this;
    }

    /**
     * Get Transporteur
     *
     * @return string
     */
    public function getTransporteur()
    {
        return $this->Transporteur;
    }


    /**
     * Set TypeService
     *
     * @param string $TypeService
     *
     * @return Header
     */
    public function setTypeService($TypeService)
    {
        $this->TypeService = $TypeService;

        return $this;
    }

    /**
     * Get TypeService
     *
     * @return string
     */
    public function getTypeService()
    {
        return $this->TypeService;
    }



    /**
     * Set issue
     *
     * @param string $issue
     *
     * @return Header
     */
    public function setIssue($issue)
    {
        $this->issue = $issue;

        return $this;
    }

    /**
     * Get issue
     *
     * @return string
     */
    public function getIssue()
    {
        return $this->issue;
    }

    /**
     * Set comments
     *
     * @param string $comments
     *
     * @return Header
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set customerCode
     *
     * @param string $customerCode
     *
     * @return Header
     */
    public function setCustomerCode($customerCode)
    {
        $this->customerCode = $customerCode;

        return $this;
    }

    /**
     * Get customerCode
     *
     * @return string
     */
    public function getCustomerCode()
    {
        return $this->customerCode;
    }

    /**
     * Set actif
     *
     * @param boolean $actif
     *
     * @return Header
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif
     *
     * @return boolean
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set customerNameRecepteur
     *
     * @param string $customerNameRecepteur
     *
     * @return Header
     */
    public function setCustomerNameRecepteur($customerNameRecepteur)
    {
        $this->customerName_recepteur = $customerNameRecepteur;

        return $this;
    }

    /**
     * Get customerNameRecepteur
     *
     * @return string
     */
    public function getCustomerNameRecepteur()
    {
        return $this->customerName_recepteur;
    }

    /**
     * Set customerSurName1Recepteur
     *
     * @param string $customerSurName1Recepteur
     *
     * @return Header
     */
    public function setCustomerSurName1Recepteur($customerSurName1Recepteur)
    {
        $this->customerSurName1_recepteur = $customerSurName1Recepteur;

        return $this;
    }

    /**
     * Get customerSurName1Recepteur
     *
     * @return string
     */
    public function getCustomerSurName1Recepteur()
    {
        return $this->customerSurName1_recepteur;
    }

    /**
     * Set customerSurName2Recepteur
     *
     * @param string $customerSurName2Recepteur
     *
     * @return Header
     */
    public function setCustomerSurName2Recepteur($customerSurName2Recepteur)
    {
        $this->customerSurName2_recepteur = $customerSurName2Recepteur;

        return $this;
    }

    /**
     * Get customerSurName2Recepteur
     *
     * @return string
     */
    public function getCustomerSurName2Recepteur()
    {
        return $this->customerSurName2_recepteur;
    }

    /**
     * Set customerAdressRecepteur
     *
     * @param string $customerAdressRecepteur
     *
     * @return Header
     */
    public function setCustomerAdressRecepteur($customerAdressRecepteur)
    {
        $this->customerAdress_recepteur = $customerAdressRecepteur;

        return $this;
    }

    /**
     * Get customerAdressRecepteur
     *
     * @return string
     */
    public function getCustomerAdressRecepteur()
    {
        return $this->customerAdress_recepteur;
    }

    /**
     * Set customerCityRecepteur
     *
     * @param string $customerCityRecepteur
     *
     * @return Header
     */
    public function setCustomerCityRecepteur($customerCityRecepteur)
    {
        $this->customerCity_recepteur = $customerCityRecepteur;

        return $this;
    }

    /**
     * Get customerCityRecepteur
     *
     * @return string
     */
    public function getCustomerCityRecepteur()
    {
        return $this->customerCity_recepteur;
    }

    /**
     * Set customerZipRecepteur
     *
     * @param string $customerZipRecepteur
     *
     * @return Header
     */
    public function setCustomerZipRecepteur($customerZipRecepteur)
    {
        $this->customerZip_recepteur = $customerZipRecepteur;

        return $this;
    }

    /**
     * Get customerZipRecepteur
     *
     * @return string
     */
    public function getCustomerZipRecepteur()
    {
        return $this->customerZip_recepteur;
    }

    /**
     * Set customerPhoneRecepteur
     *
     * @param string $customerPhoneRecepteur
     *
     * @return Header
     */
    public function setCustomerPhoneRecepteur($customerPhoneRecepteur)
    {
        $this->customerPhone_recepteur = $customerPhoneRecepteur;

        return $this;
    }

    /**
     * Get customerPhoneRecepteur
     *
     * @return string
     */
    public function getCustomerPhoneRecepteur()
    {
        return $this->customerPhone_recepteur;
    }

    /**
     * Set customerMobileRecepteur
     *
     * @param string $customerMobileRecepteur
     *
     * @return Header
     */
    public function setCustomerMobileRecepteur($customerMobileRecepteur)
    {
        $this->customerMobile_recepteur = $customerMobileRecepteur;

        return $this;
    }

    /**
     * Get customerMobileRecepteur
     *
     * @return string
     */
    public function getCustomerMobileRecepteur()
    {
        return $this->customerMobile_recepteur;
    }

    /**
     * Set customerStateRecepteur
     *
     * @param string $customerStateRecepteur
     *
     * @return Header
     */
    public function setCustomerStateRecepteur($customerStateRecepteur)
    {
        $this->customerState_recepteur = $customerStateRecepteur;

        return $this;
    }

    /**
     * Get customerStateRecepteur
     *
     * @return string
     */
    public function getCustomerStateRecepteur()
    {
        return $this->customerState_recepteur;
    }

    /**
     * Set customerCountryRecepteur
     *
     * @param string $customerCountryRecepteur
     *
     * @return Header
     */
    public function setCustomerCountryRecepteur($customerCountryRecepteur)
    {
        $this->customerCountry_recepteur = $customerCountryRecepteur;

        return $this;
    }

    /**
     * Get customerCountryRecepteur
     *
     * @return string
     */
    public function getCustomerCountryRecepteur()
    {
        return $this->customerCountry_recepteur;
    }

    /**
     * Set canceled
     *
     * @param boolean $canceled
     *
     * @return Header
     */
    public function setCanceled($canceled)
    {
        $this->canceled = $canceled;

        return $this;
    }

    /**
     * Get canceled
     *
     * @return boolean
     */
    public function getCanceled()
    {
        return $this->canceled;
    }

    /**
     * Set generated
     *
     * @param boolean $generated
     *
     * @return Header
     */
    public function setGenerated($generated)
    {
        $this->generated = $generated;

        return $this;
    }

    /**
     * Get generated
     *
     * @return boolean
     */
    public function getGenerated()
    {
        return $this->generated;
    }

    /**
     * Add detail
     *
     * @param \MRW\SiteBundle\Entity\Detail $detail
     *
     * @return Header
     */
    public function addDetail(\MRW\SiteBundle\Entity\Detail $detail)
    {
        $detail->setHeader($this);
        $this->details->add($detail);

        return $this;
    }

    /**
     * Remove detail
     *
     * @param \MRW\SiteBundle\Entity\Detail $detail
     */
    public function removeDetail(\MRW\SiteBundle\Entity\Detail $detail)
    {
        $this->details->removeElement($detail);
    }

    /**
     * Set details
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }


    /**
     * Get details
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set customer
     *
     * @param \MRW\SiteBundle\Entity\Customer $customer
     *
     * @return Header
     */
    public function setCustomer(\MRW\SiteBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return \MRW\SiteBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }



    // /**
    //  * @ORM\PrePersist
    //  */
    // public function saveDetail(){  
    //   $this->DetailTMP = $this->Detail;
    //   $this->Detail=null;
    // }
    // *
    //  * @ORM\PostPersist
    
    // public function restoreDetail(){
    //   if ($this->DetailTMP==!null){
    //     $this->Detail=$this->DetailTMP;
    //   }
    //   $this->DetailTMP=null;
    // }    

    /**
     * Set generatedCanceled
     *
     * @param boolean $generatedCanceled
     *
     * @return Header
     */
    public function setGeneratedCanceled($generatedCanceled)
    {
        $this->generatedCanceled = $generatedCanceled;

        return $this;
    }

    /**
     * Get generatedCanceled
     *
     * @return boolean
     */
    public function getGeneratedCanceled()
    {
        return $this->generatedCanceled;
    }

   

    // /**
    //  * Set trackingId
    //  *
    //  * @param string $trackingId
    //  *
    //  * @return Header
    //  */
    // public function setTrackingId($trackingId)
    // {
    //     $this->trackingId = $trackingId;

    //     return $this;
    // }

    // /**
    //  * Get trackingId
    //  *
    //  * @return string
    //  */
    // public function getTrackingId()
    // {
    //     return $this->trackingId;
    // }

    /**
     * Set trackingId
     *
     * @param \MRW\SiteBundle\Entity\TrackingId $trackingId
     *
     * @return Header
     */
    public function setTrackingId(\MRW\SiteBundle\Entity\TrackingId $trackingId = null)
    {
        $this->trackingId = $trackingId;

        return $this;
    }

    /**
     * Get trackingId
     *
     * @return \MRW\SiteBundle\Entity\TrackingId
     */
    public function getTrackingId()
    {
        return $this->trackingId;
    }

    /**
     * Set codeBarre
     *
     * @param string $codeBarre
     *
     * @return Header
     */
    public function setCodeBarre($codeBarre)
    {
        $this->codeBarre = $codeBarre;

        return $this;
    }

    /**
     * Get codeBarre
     *
     * @return string
     */
    public function getCodeBarre()
    {
        return $this->codeBarre;
    }
}
