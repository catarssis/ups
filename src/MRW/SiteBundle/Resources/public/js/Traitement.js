$(document).ready(function()
{
    $('.toggle-nav').click(function() {
          $('body').toggleClass('show-nav'); 
          return false;
    });
    $('.toggle-nav').click(function() {
        if ($('body').hasClass('show-nav')) {
            $('body').removeClass('show-nav').addClass('hide-nav');

            setTimeout(function() {
                $('body').removeClass('hide-nav');
            }, 500);

        } else {
            $('body').removeClass('hide-nav').addClass('show-nav');
        }

        return false;
    });
    // Toggle with hitting of ESC
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            $('body').toggleClass('show-nav');
            // $('body').removeClass('show-nav');
        }
    });
    if ( $('.trackingId-search').length && !$('.trackingId-search').is(':disabled') )
    {
        $('#mrw_sitebundle_header_trackingId').after('<div class="icon"><img class="hidden" src="' + IMAGES_PATH + '/delete.png" /></div>');
        if ( $('.trackingId-search').val().length > 0 ) { $('.icon img').removeClass('hidden'); }
        
        // $.ajax({
        //     method: 'POST',
        //     url: FIND_TRACKINGID_URL,
        //     data: { 'trackingId': $('.trackingId-search').val() }
        // }).done(function(response){
        //     if ( response.success ) 
        //     {
        //         $(".icon img").attr('src', IMAGES_PATH + '/mark.png');
        //     }
        //     else $(".icon img").attr('src', IMAGES_PATH + '/delete.png');
        // });

        $(document).on('keyup', '.trackingId-search', function(){
            if ( $('.trackingId-search').val().length > 0 ) { $('.icon img').removeClass('hidden'); }
            else $('.icon img').addClass('hidden');
            if ($('.trackingId-search').val().length == 12)
            {
                $.ajax({
                    method: 'POST',
                    url: FIND_TRACKINGID_URL,
                    data: { 'trackingId': $('.trackingId-search').val() }
                }).done(function(response){                   
                    if ( response.success ) 
                    {
                        $(".icon img").attr('src', IMAGES_PATH + '/mark.png');
                    }
                    else $(".icon img").attr('src', IMAGES_PATH + '/delete.png');
                });
            }
        });
    }

var $collectionHolder;

// setup an "add a tag" link
var $addTagLink = $('<a href="#" class="add_tag_link">Ajouter un detail</a>');
var $newLinkLi = $('<tr></tr>').append($addTagLink);

jQuery(document).ready(function() {
    // Get the ul that holds the collection of tags
    $collectionHolder = $('table.details');

    // add the "add a tag" anchor and li to the tags ul
    $collectionHolder.append($newLinkLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    $addTagLink.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();

        // add a new tag form (see next code block)
        addTagForm($collectionHolder, $newLinkLi);

        if ( $(".productId-class").length )
        {
            $.each($(".productId-class"), function(index, val) 
            {
                if ( $(this).val() == "" ) $(this).val('00917_1970BULTOPAXD');    
            });
            
        }
    });
});

function addTagForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');
    prototype = prototype.replace('<div id="mrw_sitebundle_header_details___name__">','');
    prototype = prototype.replace(/<\/div>$/,'');
    prototype = prototype.replace(/<div>/g,'<td>');
    prototype = prototype.replace(/<\/div>/g,'</td>');
    console.log(prototype);

    // get the new index
    var index = $collectionHolder.data('index');

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a tag" link li
    var $newFormLi = $('<tr></tr>').append(newForm);
    $newLinkLi.before($newFormLi);
}

});
