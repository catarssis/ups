<?php

namespace MRW\SiteBundle\Services;



class SFTPConnections
{
	
	private $oConnection;
    private $oSftp;

    public function __construct($sHost, $sPort=22)
    {
        $this->oConnection = @ssh2_connect($sHost, $sPort);
        if (! $this->oConnection)
            throw new Exception("Could not connect to $sHost on port $sPort.");
    }

    public function login($sUsername, $sPassword)
    {
        if (! @ssh2_auth_password($this->oConnection, $sUsername, $sPassword))
            throw new Exception("Could not authenticate with username $sUsername " .
                                "and password $sPassword.");

        $this->oSftp = @ssh2_sftp($this->oConnection);
        if (! $this->oSftp)
            throw new Exception("Could not initialize SFTP subsystem.");
    }

    public function uploadFile($sLocalFile, $sRemoteFile)
    {
        $oSftp = $this->oSftp;
        $oStream = @fopen("ssh2.sftp://$oSftp$sRemoteFile", 'w');

        if (! $oStream)
            throw new Exception("Could not open file: $sRemoteFile");

        $sDataToSend = @file_get_contents($sLocalFile);
        if ($sDataToSend === false)
            throw new Exception("Could not open local file: $sLocalFile.");

        if (@fwrite($oStream, $sDataToSend) === false)
            throw new Exception("Could not send data from file: $sLocalFile.");

        @fclose($oStream);
    }

    function scanFilesystem($sRemoteFile) {
              $sFtp = $this->sFtp;
            $sDirectory = "ssh2.sftp://$sFtp$sRemoteFile";  
              $aTempArray = array();
            $oHandler = opendir($sDirectory);
          // List all the files
            while (false !== ($oFile = readdir($oHandler))) {
            if (substr("$oFile", 0, 1) != "."){
              if(is_dir($oFile)){
//                $tempArray[$file] = $this->scanFilesystem("$dir/$file");
               } else {
                 $aTempArray[]=$oFile;
               }
             }
            }
           closedir($oHandler);
          return $aTempArray;
        }    

    public function receiveFile($sRemoteFile, $sLocalFile)
    {
        $sFtp = $this->sFtp;
        $oStream = @fopen("ssh2.sftp://$sFtp$sRemoteFile", 'r');
        if (! $oStream)
            throw new Exception("Could not open file: $sRemoteFile");
        $iSize = $this->getFileSize($sRemoteFile);            
        $sContents = '';
        $iRead = 0;
        $iLength = $iSize;
        while ($iRead < $iLength && ($oBuffer = fread($oStream, $iLength - $iRead))) {
          $iRead += strlen($oBuffer);
          $sContents .= $oBuffer;
        }        
        file_put_contents ($sLocalFile, $sContents);
        @fclose($oStream);
    }
        
    public function deleteFile($sRemoteFile){
      $sFtp = $this->sFtp;
      unlink("ssh2.sftp://$sFtp$sRemoteFile");
    }


}

?>
