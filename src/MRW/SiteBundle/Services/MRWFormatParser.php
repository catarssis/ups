<?php

namespace MRW\SiteBundle\Services;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;



class MRWFormatParser
{
	
	private $oContainer;
	private $sFolderPathIADO;
	private $sFolderPathIADI;
	private $sMaxCharPerLine = array(
	                        'read' => 88, 
	                        'write' => '');
	private $aFields = array(
			'recordType' => array('from' => 1, 'to' => 1),
			'orderType' => array('from' => 2, 'to' => 2),
			'orderCode' => array('from' => 3, 'to' => 22),
			'customerCode' => array('from' => 23, 'to' => 42),
			'serviceDate' => array('from' => 43, 'to' => 50),
			'statusDate' => array('from' => 51, 'to' => 58),
			'trackingId' => array('from' => 59, 'to' => 78),
			'status' => array('from' => 79, 'to' => 83),
			'substatus' => array('from' => 84, 'to' => 88)
		);
	private $aFieldsToWrite = array(
		'canceled' => array(
		    'recordType'	=>	1,
			'orderType'		=>	20,
			'orderCode'		=>	20,
			'customerCode'	=>	8,
			'issue'			=>	20,
			'comments'		=>	30
		),
		'header' => array(
		    'recordType'						=>	1,
			'orderType'							=>	1,
			'orderCode'							=>	20,
			'customerCode'						=>	20,
			'serviceDate'						=>	8,
			'transmitionDate'					=>	8,
			'preparationCenter'					=>	20,
			'customerName_recepteur'			=>	30,
			'customerSurname1_recepteur'		=>	30,
			'customerSurname2_recepteur'		=>	30,
			'customerAdress_recepteur'			=>	80,
			'customerCity_recepteur'			=>	20,
			'customerZip_recepteur'				=>	20,
			'customerPhone_recepteur'			=>	20,
			'customerMobile_recepteur'			=>	20,
			'customerState_recepteur'			=>	20,
			'customerCountry_recepteur'			=>	2,
			'vatId'								=>	9,
			'weight'							=>	12,
			'volume'							=>	13,
			'bundles'							=>	3,
			'refund'							=>	1,
			'refundValue'						=>	12,
			'comments1'							=>	50,
			'trackingId'						=>	12,
			'deliveryType'						=>	1,
			'comments2'							=>	50,
			'email'								=>	50,
			'sms'								=>	20,
			'echange'							=>	1,
			'transporteur'						=>	10,
			'typeService'						=>  20,
			'campaniaMRW'						=>  20,
			'entregaSabado'						=>  1
		),
		'lines' => array(
		    'recordType'						=>	1,
			'orderCode'							=>	20,
			'productId'							=>	20,
			'quantity'							=>	9,
			'unitCost'							=>	12,
			'salePrice'							=>	12,
			'customerCode'						=>	20,
			'warehouse'							=>	20
		)
		// 'uncanceled' => array(
		// 	'recordType'		=> 1,
		// 	'orderType'			=> 1,
		// 	'orderCode'			=> 20,
		// 	'customerCode'		=> 8,
		// 	'serviceDate'		=> 8,
		// 	'transmitionDate'	=> 8,
		// 	'preparationCenter'	=> 20,
		// 	'customerName'		=> 30,
		// 	'customerSurname1'	=> 30,
		// 	'customerSurname2'	=> 30,
		// 	'customerAddress'	=> 80,
		// 	'customerCity'		=> 20,
		// 	'customerZIP'		=> 20,
		// 	'customerPhone'		=> 20,
		// 	'customerMobile'	=> 20,
		// 	'customerState'		=> 20,
		// 	'customerCountry'	=> 2,
		// 	'vatId'				=> 13,
		// 	'weight'			=> 12,
		// 	'volume'			=> 13,
		// 	'bundles'			=> 3,
		// 	'refund'			=> 1,
		// 	'refundValue'		=> 12,
		// 	'commentsI'			=> 50	,
		// 	'trackingId'		=> 12,
		// 	'deliveryType'		=> 1,
		// 	'commentsII'		=> 50,
		// 	'email'				=> 50
		//  )
	);

	private $aFieldsTypeInt = array(
		'bundles',
		'quantity'
	);

	public function __construct($oContainer, $sGlobalFolderPath)
	{
		$this->oContainer = $oContainer;
		$this->sFolderPathIADO = realpath($sGlobalFolderPath . '/../web/files/IADO' );
		$this->sFolderPathIADI = realpath($sGlobalFolderPath . '/../web/files/IADI' );
		$this->sFolderPathIADR = realpath($sGlobalFolderPath . '/../web/files/IADR' );
	}

	public function getFilestoRead()
	{
		$finder = new Finder();
        return $finder->files()->in($this->sFolderPathIADO);
	}

	public function read($sFileName)
	{
		$iRow = 1;
		
		$aDataToCollect = array();
		if (($oHandler = fopen($this->sFolderPathIADO . '/' . $sFileName, "r")) !== FALSE) {
		    while (($aData = fgetcsv($oHandler, (int)$this->sMaxCharPerLine['read'] + 1)) !== FALSE) 
		    {
		    	if ( mb_strlen($aData[0]) > 0 ) $aDataToCollect[$iRow] = str_split($aData[0]);
		        $iRow++;
		    }
		    fclose($oHandler);
		}

		$aFinalData = array();
		// Boucle sur chaque ligne
		foreach ($aDataToCollect as $_iKey => $_sValue) 
		{
			$aFinalData[$_iKey] = $this->aFields;
			// Boucle sur les champs
			foreach ($this->aFields as $sField => $sValue) 
			{
				$aFinalData[$_iKey][$sField] = '';
				// Boucle sur chaque caractère de chaque ligne
				foreach ($_sValue as $__iKey => $__sValue) 
				{
					if ( $__iKey + 1 >= $sValue['from'] && $__iKey + 1 <= $sValue['to'] )
					{
						$aFinalData[$_iKey][$sField] .= trim($__sValue);		
					}	
				}
				
			}

		}

		return $aFinalData;
	}

	public function write($aData, $sFileName, $bCanceled = false)
	{
		mb_internal_encoding("UTF-8");
		$sCancelationValue = ( $bCanceled ) ? 'canceled' : 'header';
		$sToWrite = "";

		// Boucle sur les valeurs
		foreach ($aData as $_iKey => $_sValue) 
		{
			// Boucle sur les propriétés
			//foreach ($this->aFieldsToWrite[$sCancelationValue] as $iKey => $sValue) 
			foreach ($_sValue as $iKey => $sValue) 
			{
				// Si la propriété est un tableau de lignes
				if ( $iKey == 'lines' )
				{
					$sToWrite .= "\r\n";
					// Boucle sur les lignes
					foreach ($sValue as $__iKey => $__sValue) 
					{
						// Boucle sur les propriétés de chaque ligne
						foreach ($__sValue as $key => $value) 
						{											
							// Si la propriété de la ligne figure dans les colonnes allowed des lignes
							if ( array_key_exists($key, $this->aFieldsToWrite['lines']) )
							{
								$value = trim($value);

								if ( $key == 'salePrice' || $key == 'unitCost' ) 
								{
		                        	if ( mb_strlen($value, "UTF-8") < $this->aFieldsToWrite['lines'][$key] )
		                        	{
		                        		$value = str_replace(',', '.', $value);
		                        		
		                        		if ( strpos($value, '.') != false )
		                        		{
		                        			$avalue = explode('.', $value);

		                        			$sToWrite .= str_pad($avalue[0], 9, '0', STR_PAD_LEFT) . '.' . str_pad($avalue[1], 2, '0', STR_PAD_RIGHT);
		                        		}
		                        		else
		                        		{
		                        			$sToWrite .= str_pad($value, 9, '0', STR_PAD_LEFT) . '.' . str_pad('', 2, '0', STR_PAD_RIGHT);
		                        		}
		                        	}
								}
								else if ( in_array($key, $this->aFieldsTypeInt) ) 
								{
									$sToWrite .= str_pad($value, (int)$this->aFieldsToWrite['lines'][$key], '0', STR_PAD_LEFT);
								}
								else
								{
									$sToWrite .= str_pad($value, (int)$this->aFieldsToWrite['lines'][$key], ' ', STR_PAD_RIGHT);
								}
							}
						}	

						//$sToWrite .= "\r\n";
					}
				}
				else
				{

					// Si la propriété du header figure dans les colonnes allowed
					if ( array_key_exists($iKey, $this->aFieldsToWrite[$sCancelationValue]) )
					{   
						// Ecrire la valeur de la propriété de la ligne
						if ( $sValue instanceof \DateTime)
                        {
                            $sValue = $sValue->format('dmY');
                        }

                        $sValue = trim($sValue);

                        if ( $iKey == 'weight' || $iKey == 'volume' || $iKey == 'refundValue' ) 
                        {
                        	if ( $iKey == 'volume' )
                				$aCompletes = array('6', '6');
                			else if ( $iKey == 'weight' )
                				$aCompletes = array('8', '3');
                			else
                				$aCompletes = array('9', '2');

                        	if ( mb_strlen($sValue, "UTF-8") < $this->aFieldsToWrite[$sCancelationValue][$iKey] )
                        	{
                        		$sValue = str_replace(',', '.', $sValue);
                        		
                        		if ( strpos($sValue, '.') != false )
                        		{
                        			$aValue = explode('.', $sValue);

                        			$sToWrite .= str_pad($aValue[0], $aCompletes[0], '0', STR_PAD_LEFT) . '.' . str_pad($aValue[1], $aCompletes[1], '0', STR_PAD_RIGHT);
                        		}
                        		else
                        		{
                        			$sToWrite .= str_pad($sValue, $aCompletes[0], '0', STR_PAD_LEFT) . '.' . str_pad('', $aCompletes[1], '0', STR_PAD_RIGHT);

                        		}

                        	}
                        }
                        else if ( in_array($iKey, $this->aFieldsTypeInt) ) 
                        {
							// Si la taille de la valeur de la propriété du header est inférieure à ce qui est allowed
							if ( mb_strlen($sValue, "UTF-8") < $this->aFieldsToWrite[$sCancelationValue][$iKey] )
							{
								// On complète la taille par des espaces
								for ($i=0; $i < ((int)$this->aFieldsToWrite[$sCancelationValue][$iKey] - mb_strlen($sValue, "UTF-8")) ; $i++) 
								{
									$sToWrite .= '0';
								}
							}
							$sToWrite .= $sValue;
                        }
                        else
                        {
                        	$sToWrite .= $sValue;
                        	// Si la taille de la valeur de la propriété du header est inférieure à ce qui est allowed
							if ( mb_strlen($sValue, "UTF-8") < $this->aFieldsToWrite[$sCancelationValue][$iKey] )
							{
								// On complète la taille par des espaces
								for ($i=0; $i < ((int)$this->aFieldsToWrite[$sCancelationValue][$iKey] - mb_strlen($sValue, "UTF-8")) ; $i++) 
								{
									$sToWrite .= ' ';
								}
							}
                        }
						
					}		
				}
			}
			$sToWrite .= "\r\n";
		}
		
		$oFileSystem = new Filesystem();

		$oFileSystem->dumpFile($this->sFolderPathIADI . '/' . $sFileName , utf8_decode($sToWrite));
	}

}

?>
