<?php

namespace MRW\SiteBundle\Services;



class GlobalFunctions
{
	
	private $oContainer;

	public function __construct($oContainer)
	{
		$this->oContainer = $oContainer;
	}

	// Find occurences in string using an array of needles
	public function strpos_arr($haystack, $needle, $add = "") 
	{
		if(!is_array($needle)) $needle = array($needle);

		foreach($needle as $what) 
		{
			if(($pos = strpos($haystack, $what . $add))!==false) return $pos;
		}

		return false;
	}

	// Parse Tree Parent Child in Array
	public function parseTree($aArray, $root = null) 
	{
	    $aFinalData = array();
	    $aTree = array();
	    
	    $aTree['Children'] = array();
	    //# Traverse the tree and search for direct children of the root
	    foreach($aArray as $iKey => $sValue) 
	    {
	        $aTree['Children'][$sValue->getId()] = $sValue;
	    }

	    $aTree = array();
	    /* Most datasets in the wild are enumerative arrays and we need associative array
	       where the same ID used for addressing parents is used. We make associative
	       array on the fly */
	    $aReferences = array();
	    foreach ($aTree['Children'] as $iKey => &$sValue) 
	    {
	        // Add the node to our associative array using it's ID as key
	        $aReferences[$sValue->getId()] = &$sValue;
	 
	        // It it's a root node, we add it directly to the tree
	        if (is_null($sValue->getPageParente())) {
	            $aTree[$sValue->getId()] = &$sValue;
	        } else {
	        	$sValue->pages_enfant = array();
	            // It was not a root node, add this node as a reference in the parent.
	            $aReferences[$sValue->getPageParente()->getId()]->pages_enfant[$sValue->getId()] = &$sValue;
	        }
	    }
		 
		return $aTree;
	}

}

?>
