<?php

namespace MRW\SiteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

// use MRW\SiteBundle\Entity\TrackingId;
use MRW\SiteBundle\Entity\TrackingId;
use MRW\SiteBundle\Form\TrackingIdType;

/**
 * TrackingId controller.
 *
 */
class TrackingIdController extends Controller
{

    /**
     * Lists all TrackingId entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MRWSiteBundle:TrackingId')->findAll();

        return $this->render('MRWSiteBundle:TrackingId:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new TrackingId entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new TrackingId();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('trackingid_show', array('id' => $entity->getId())));
        }

        return $this->render('MRWSiteBundle:TrackingId:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a TrackingId entity.
     *
     * @param TrackingId $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(TrackingId $entity)
    {
        $form = $this->createForm(new TrackingIdType(), $entity, array(
            'action' => $this->generateUrl('trackingid_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new TrackingId entity.
     *
     */
    public function newAction()
    {
        $entity = new TrackingId();
        $form   = $this->createCreateForm($entity);

        return $this->render('MRWSiteBundle:TrackingId:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a TrackingId entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MRWSiteBundle:TrackingId')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TrackingId entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MRWSiteBundle:TrackingId:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing TrackingId entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MRWSiteBundle:TrackingId')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TrackingId entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MRWSiteBundle:TrackingId:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a TrackingId entity.
    *
    * @param TrackingId $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(TrackingId $entity)
    {
        $form = $this->createForm(new TrackingIdType(), $entity, array(
            'action' => $this->generateUrl('trackingid_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing TrackingId entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MRWSiteBundle:TrackingId')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TrackingId entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('trackingid_edit', array('id' => $id)));
        }

        return $this->render('MRWSiteBundle:TrackingId:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a TrackingId entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MRWSiteBundle:TrackingId')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find TrackingId entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('trackingid'));
    }

    /**
     * Creates a form to delete a TrackingId entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('trackingid_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function findAction(Request $oRequest)
    {
        $oManager = $this->getDoctrine()->getManager();
        $oRepository = $oManager->getRepository('MRWSiteBundle:TrackingId');
        $oTrackingId = $oRepository->findOneBy(array('trackingId' => trim($oRequest->get('trackingId')), 'used' => false));

        if ( $oTrackingId instanceof TrackingId ) return new JsonResponse(array('success' => true));
        else return new JsonResponse(array('success' => false));
    }
}
