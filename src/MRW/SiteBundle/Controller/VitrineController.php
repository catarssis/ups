<?php

namespace MRW\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Youtubedl\Youtubedl;
use PHPVideoToolkit\Video;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Session\Session;

use MRW\SiteBundle\Entity\Page;
use MRW\SiteBundle\Entity\SiteConf;

class VitrineController extends Controller
{
    private $aFieldsToWrite = array(
        'canceled' => array(
            'recordType'    =>  1,
            'orderType'     =>  20,
            'orderCode'     =>  20,
            'customerCode'  =>  8,
            'issue'         =>  20,
            'comments'      =>  30
        ),
        'header' => array(
            'recordType'                        =>  1,
            'orderType'                         =>  1,
            'orderCode'                         =>  20,
            'customerCode'                      =>  20,
            'serviceDate'                       =>  8,
            'transmitionDate'                   =>  8,
            'preparationCenter'                 =>  20,
            'customerName_recepteur'            =>  30,
            'customerSurname1_recepteur'        =>  30,
            'customerSurname2_recepteur'        =>  30,
            'customerAdress_recepteur'          =>  80,
            'customerCity_recepteur'            =>  20,
            'customerZip_recepteur'             =>  20,
            'customerPhone_recepteur'           =>  20,
            'customerMobile_recepteur'          =>  20,
            'customerState_recepteur'           =>  20,
            'customerCountry_recepteur'         =>  2,
            'vatId'                             =>  9,
            'weight'                            =>  12,
            'volume'                            =>  13,
            'bundles'                           =>  3,
            'refund'                            =>  1,
            'refundValue'                       =>  12,
            'comments1'                         =>  50,
            'trackingId'                        =>  12,
            'deliveryType'                      =>  1,
            'comments2'                         =>  50,
            'email'                             =>  50,
            'sms'                               =>  20,
            'echange'                           =>  1,
            'transporteur'                      =>  10,
            'typeService'                       =>  20,
            'campaniaMRW'                       =>  20,
            'entregaSabado'                     =>  1,
            'details'                           =>  0
        ),
        'lines' => array(
            'recordType'                        =>  1,
            'orderCode'                         =>  20,
            'productId'                         =>  20,
            'quantity'                          =>  9,
            'unitCost'                          =>  12,
            'salePrice'                         =>  12,
            'customerCode'                      =>  20,
            'warehouse'                         =>  20
        )
    );


    public function indexAction()
    {
        // //$a = $this->get('MRWFormatParser')->read('IADO_726_1355_19122013200808.txt');
        // $aData = array(
        //     0 => array(
        //         'recordType'        => 'a',
        //         'orderType'         => 'a',  
        //         'orderCode'         => 'a',
        //         'customerCode'      => 'a',  
        //         'serviceDate'       => 'a',  
        //         'transmitionDate'   => 'a',
        //         'preparationCenter' => 'a',
        //         'customerName'      => 'a',
        //         'customerSurname1'  => 'a',
        //         'customerSurname2'  => 'a',
        //         'customerAddress'   => 'a',
        //         'customerCity'      => 'a',
        //         'customerZIP'       => 'a',
        //         'customerPhone'     => 'a',
        //         'customerMobile'    => 'a',
        //         'customerState'     => 'a',
        //         'customerCountry'   => 'a',
        //         'vatId'             => 'a',
        //         'weight'            => 'a',
        //         'volume'            => 'a',
        //         'bundles'           => 'a',
        //         'refund'            => 'a',
        //         'refundValue'       => 'a',
        //         'commentsI'         => 'a',  
        //         'trackingId'        => 'a',
        //         'deliveryType'      => 'a',
        //         'commentsII'        => 'a',
        //         'email'             => 'a'
        //     ),
        //     1 => array(
        //         'recordType'        => 'a',
        //         'orderType'         => 'a',  
        //         'orderCode'         => 'a',
        //         'customerCode'      => 'a',  
        //         'serviceDate'       => 'a',  
        //         'transmitionDate'   => 'a',
        //         'preparationCenter' => 'a',
        //         'customerName'      => 'a',
        //         'customerSurname1'  => 'a',
        //         'customerSurname2'  => 'a',
        //         'customerAddress'   => 'a',
        //         'customerCity'      => 'a',
        //         'customerZIP'       => 'a',
        //         'customerPhone'     => 'a',
        //         'customerMobile'    => 'a',
        //         'customerState'     => 'a',
        //         'customerCountry'   => 'a',
        //         'vatId'             => 'a',
        //         'weight'            => 'a',
        //         'volume'            => 'a',
        //         'bundles'           => 'a',
        //         'refund'            => 'a',
        //         'refundValue'       => 'a',
        //         'commentsI'         => 'a',  
        //         'trackingId'        => 'a',
        //         'deliveryType'      => 'a',
        //         'commentsII'        => 'a',
        //         'email'             => 'a'
        //     ),
        //     2 => array(
        //         'recordType'        => 'a'
        //     )
        // );
        // $sCompleteCurrentDateTime = new \DateTime();
        // $sCompleteCurrentDateTime = $sCompleteCurrentDateTime->format('dmYHis');
        // $this->get('MRWFormatParser')->write($aData, 'IARI_726_1355_' . $sCompleteCurrentDateTime . '.txt');

        //$oPage = $this->getDoctrine()->getRepository('MRWSiteBundle:Page')->getPageActifByParam('name', 'Index');            
        return $this->render('MRWSiteBundle:Pages:index.html.twig');        
    }

    public function generateCanceledHeaderAction()
    {
        $aHeaders = $this->getDoctrine()->getRepository('MRWSiteBundle:Header')->getCanceledHeaders();

        $session = $this->get('session');

        if (count($aHeaders) > 0)
        {

            $aNewHeaders = array();

        $em = $this->getDoctrine()->getManager();

        foreach ( $aHeaders as $k => $aHeader)
        {                
            
            foreach ($aHeader->getAllProperties() as $key => $oHeader) 
            {
                 
                if ($key != 'details')
                {

                    if ($oHeader instanceof \DateTime)
                        {
                            
                            $result = $oHeader->format('dmYHis');
                            
                            $aNewHeaders[$k][$key] = $result;
                        }

                    else                    
                        $aNewHeaders[$k][$key] = $oHeader;
                }               
            }

            $aHeader->setGeneratedCanceled(true);
            $em->flush();
            $session->getFlashBag()->add('noticeGeneratedHeader', 'Headers annulés générés avec succès !');

        }

        $sCompleteCurrentDateTime = new \DateTime();
        $sCompleteCurrentDateTime = $sCompleteCurrentDateTime->format('dmYHis');
        $this->get('MRWFormatParser')->write($aNewHeaders, 'IARI_726_1355_' . $sCompleteCurrentDateTime . '.txt', true);

        }
        else
        {
            $session->getFlashBag()->add('noticeGeneratedHeader', 'Aucun Header annulé a générer');
        }

       

        return $this->redirect($this->generateUrl('header'));
    }

    public function landingAction()
    {
        return false;
    }

    public function generateHeaderAction()
    {
        $aHeaders = $this->getDoctrine()->getRepository('MRWSiteBundle:Header')->getHeaders();
        $session = $this->get('session');

    if (count($aHeaders) > 0)

       { $aNewHeaders = array();
       
               $em = $this->getDoctrine()->getManager();
       
               foreach ( $aHeaders as $k => $aHeader)
               {

                    $aHeader->setGenerated(true);
                   $dNewDate = new \DateTime();
                   $aHeader->setTransmitionDate($dNewDate);
                   $em->persist($aHeader);
                     
                   foreach ($this->aFieldsToWrite['header'] as $key => $oHeader) 
                   //foreach ($aHeader->getAllProperties() as $key => $oHeader) 
                   {
                        
                        $sGetter = 'get' . ucfirst($key);
                        //$str = "abc_def_ghi_jkl";
                        $sGetter = preg_replace_callback("/(?:^|_)([a-z])/", function($matches) {
                        //       Start or underscore    ^      ^ lowercase character
                            return strtoupper($matches[1]);
                        }, $sGetter);

                        $sGetter = lcfirst($sGetter);

                       if ($key != 'details')
                       {
       
                            if ( $aHeader->$sGetter() instanceof \DateTime)
                            {
                                $result = $aHeader->$sGetter('dmYHis');
                               
                                $aNewHeaders[$k][$key] = $result;
                            }
       
                           else                    
                               $aNewHeaders[$k][$key] = $aHeader->$sGetter();
                       }               
                       else
                           foreach ($aHeader->$sGetter() as $dKey => $oDetail) 
                           {
                                foreach ($this->aFieldsToWrite['lines'] as $key => $oHeader) 
                                //foreach ($oDetail->getAllProperties() as $fKey => $value) 
                                {
        
                                    if ($oDetail->getAllProperties()[$key] instanceof \DateTime)
                                    {                                
                                        $result2 = $oDetail->getAllProperties()[$key]->format('dmYHis');
        
                                        $aNewHeaders[$k]['lines'][$dKey][$key] = $result2;
                                    }
        
                                    else if(!is_object($oDetail->getAllProperties()[$key]) )
                                    {
                                        $aNewHeaders[$k]['lines'][$dKey][$key] = $oDetail->getAllProperties()[$key];                                 
                                    }
                                }
                           }
       
                   }

       
                   
                   
       
                   
               }
               $em->flush();
               $session->getFlashBag()->add('noticeGeneratedHeader', 'Headers générés avec succès !');

                   foreach ($aNewHeaders as $key => $value) 
                   {
                       if (isset($aNewHeaders[$key]['lines']) && count($aNewHeaders[$key]['lines']) > 0)
                       {
                         $aDetail = $aNewHeaders[$key]['lines'];
                         unset($aNewHeaders[$key]['lines']);
                         $aNewHeaders[$key]['lines'] = $aDetail;                
                       }
                   }            
               
               $sCompleteCurrentDateTime = new \DateTime();
               $sCompleteCurrentDateTime = $sCompleteCurrentDateTime->format('dmYHis');
               
               $this->get('MRWFormatParser')->write($aNewHeaders, 'IADI_726_1355_' . $sCompleteCurrentDateTime . '.txt');
           }

           else
           {
                $session->getFlashBag()->add('noticeGeneratedHeader', 'Aucun Header a générer');
           }

        return $this->redirect($this->generateUrl('header'));
       
    }

    public function printHeaderAction($id,$key, $format)
    {

        $oDetail = $this->getDoctrine()->getRepository('MRWSiteBundle:Detail')->findOneBy(array('id' => $id));
        
        $oRouta = $this->getDoctrine()->getRepository('MRWSiteBundle:Routa')->findOneBy(array('codePays' => $oDetail->getHeader()->getCustomerCountryRecepteur(), 'codePostal' => $oDetail->getHeader()->getCustomerZipRecepteur()));

        $sCodeRoute = str_pad($oRouta->getCodeRoute(), 2, '0', STR_PAD_LEFT);

        $sCodeBarre = '2' . $oRouta->getCodeAgence() . '02680' . '0' . substr($oDetail->getHeader()->getTrackingId()->getTrackingId(), -7) . str_pad((int)$key + 1, 3, "0", STR_PAD_LEFT);

        $sTE = false;

        $sBottomImage = false;

        switch ($oDetail->getHeader()->getTypeService()) 
        {
            case 'URGENTE_10:00':
                $sTE = '0100';
                $sBottomImage = 'URGENTE_1000';
                break;
            case 'URGENTE_12:00':
                $sTE = '0600';
                $sBottomImage = 'URGENTE_1200';
                break;
            // case 'URGENTE_14:00':
            //     $sTE = '2100';
            //     $sBottomImage = 'URGENTE_1400';
            //     break;
            case 'URGENTE_19:00':
                $sTE = '0400';
                $sBottomImage = 'URGENTE_1900';
                break;
            case 'ECOMMERCE':
                $sTE = '2400';
                $sBottomImage = 'ECOMMERCE';
                break;
            // case 'SERVICIO URGENTE 8:30H EXPEDICIÓN':
            //     $sTE = '0400';
            //     $sBottomImage = 'SERVICIO_URGENTE_830H_EXPEDICION';
            //     break;
            // case 'URGENTE_10EXP':
            //     $sTE = '0400';
            //     $sBottomImage = 'URGENTE_10EXP';
            //     break;
            // case 'URGENTE_12EXP':
            //     $sTE = '0400';
            //     $sBottomImage = 'URGENTE_12EXP';
            //     break;
            // case 'URGENTE_14EXP':
            //     $sTE = '0400';
            //     $sBottomImage = 'URGENTE_14EXP';
            //     break;
            // case 'URGENTE_19EXP':
            //     $sTE = '0400';
            //     $sBottomImage = 'URGENTE_19EXP';
            //     break;
            case 'URGENTE_8:30':
                $sTE = '0100';
                $sBottomImage = 'URGENTE_830';
                break;
            case 'MARITIMO_BALEARES':
                $sTE = '0100';
                $sBottomImage = 'MARITIMO_BALEARES';
                break;
            default:
                $sTE = false;
                break;
        }

        return $this->render('MRWSiteBundle:Pages:print_header.html.twig', ['oDetail' => $oDetail, 'key' => $key, 'sCodeBarre' => $sCodeBarre, 'oRouta' => $oRouta, 'sCodeRoute' => $sCodeRoute, 'sTE' => $sTE, 'sBottomImage' => $sBottomImage, 'format' => $format  ]);
    }
}
