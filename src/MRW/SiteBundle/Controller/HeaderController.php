<?php

namespace MRW\SiteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MRW\SiteBundle\Entity\Header;
use MRW\SiteBundle\Entity\Routa;
use MRW\SiteBundle\Entity\TrackingId;
use MRW\SiteBundle\Entity\Detail;
use MRW\SiteBundle\Entity\Config;
use MRW\SiteBundle\Form\HeaderType;
use Symfony\Component\Finder\Finder;

/**
 * Header controller.
 *
 */
class HeaderController extends Controller
{

    /**
     * Lists all Header entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MRWSiteBundle:Header')->findBy(array('actif' => true));

        return $this->render('MRWSiteBundle:Header:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Header entity.
     *
     */
    public function createAction(Request $oRequest)
    { 
        $sGeneratedOrderCode = null;

        $oEm = $this->getDoctrine()->getManager();
        $oHeader = new Header();

        $oForm = $this->createCreateForm($oHeader);

        $session = $this->get('session');

        $oTrackingId = $oEm->getRepository('MRWSiteBundle:TrackingId')->findOneBy(array('trackingId' => $oRequest->request->get('mrw_sitebundle_header')['trackingId'], 'used' => false));

        $oForm->handleRequest($oRequest);

        if ( !($oTrackingId instanceof TrackingId) ) 
        {
            $session->getFlashBag()->add('noticeGeneratedHeader', 'Tracking ID déjà utilisé');
            return $this->render('MRWSiteBundle:Header:new.html.twig', array(
                'entity' => $oHeader,
                'form'   => $oForm->createView(),
            ));
        }

        if ($oForm->isValid() && $oTrackingId instanceof TrackingId) 
        {
            // ladybug_dump($oHeader->getDetails());
            // die();

            if($oHeader->getDetails()->count() == 0)
            {
                $session->getFlashBag()->add('noticeGeneratedHeader', 'Merci d\'ajouter des details !');
                return $this->render('MRWSiteBundle:Header:new.html.twig', array(
                    'entity' => $oHeader,
                    'form'   => $oForm->createView(),
                ));
            }
            $oConfig = $oEm->getRepository('MRWSiteBundle:Config')->getLastConfig();
      
            if($oConfig != null && $oConfig->getAnnee() != null && $oConfig->getAnnee() === (int)date("Y") )
            {
                $sGeneratedOrderCode1 = $oConfig->getCount()+1;
                $oConfig->setCount($sGeneratedOrderCode1);
                $oEm->persist($oConfig);
            }
            else
            {
                $oNewConfig = new Config();
                $oNewConfig->setAnnee((int)date("Y"));
                $oNewConfig->setCount(1);
                $oEm->persist($oNewConfig);
                $sGeneratedOrderCode1 = 1;
            }
            
            $oTrackingId->setUsed(true);

            $oHeader->setTrackingId($oTrackingId);

            $oRouta = $oEm->getRepository('MRWSiteBundle:Routa')->findOneBy(array('codePays' => $oHeader->getCustomerCountryRecepteur(), 'codePostal' => $oHeader->getCustomerZipRecepteur()));

            

            if ( !($oRouta instanceof Routa) )
            {
                $session->getFlashBag()->add('noticeGeneratedHeader', 'Routa Incorrecte !');
                return $this->render('MRWSiteBundle:Header:new.html.twig', array(
                    'entity' => $oHeader,
                    'form'   => $oForm->createView()
                ));
            }

            $sGeneratedOrderCode2 = "E". substr(((string)date("Y")), 2);

            for ($i=0; $i < (5 - strlen((string)$sGeneratedOrderCode1)) ; $i++) 
            { 
                $sGeneratedOrderCode2 .= "0";
            }

            $sGeneratedOrderCodeFinal = $sGeneratedOrderCode2 . $sGeneratedOrderCode1;

            $oHeader->setOrderCode($sGeneratedOrderCodeFinal);
            $oEm->persist($oHeader);            

            $oEm->flush();
            return $this->redirect($this->generateUrl('header_show', array('id' => $oHeader->getId())));
        }

        return $this->render('MRWSiteBundle:Header:new.html.twig', array(
            'entity' => $oHeader,
            'form'   => $oForm->createView(),
        ));
    }

    /**
     * Creates a form to create a Header entity.
     *
     * @param Header $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Header $entity)
    {
        $form = $this->createForm(new HeaderType(), $entity, array(
            'action' => $this->generateUrl('header_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Header entity.
     *
     */
    public function newAction()
    {
        $entity = new Header();
        $form   = $this->createCreateForm($entity);

        return $this->render('MRWSiteBundle:Header:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Header entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MRWSiteBundle:Header')->findOneBy(array('id' => $id, 'actif' => true));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Header entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MRWSiteBundle:Header:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Header entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MRWSiteBundle:Header')->findOneBy(array('id' => $id, 'actif' => true));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Header entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        if (is_object($entity->getTrackingId()))
        $editForm->get('trackingId')->setData($entity->getTrackingId()->getTrackingId());

        return $this->render('MRWSiteBundle:Header:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Header entity.
    *
    * @param Header $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Header $entity)
    {
        $form = $this->createForm(new HeaderType(), $entity, array(
            'action' => $this->generateUrl('header_update', array('id' => $entity->getId())),
            'method' => 'POST',
        ));

        /*$form->add('submit', 'submit', array('label' => 'Update'));*/

        return $form;
    }
    /**
     * Edits an existing Header entity.
     *
     */
    public function updateAction(Request $oRequest, $id)
    {
        

        $em = $this->getDoctrine()->getManager();

        $oHeader = $em->getRepository('MRWSiteBundle:Header')->findOneBy(array('id' => $id, 'actif' => true));

        if (!$oHeader) {
            throw $this->createNotFoundException('Unable to find Header entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($oHeader);

        $editForm->handleRequest($oRequest);

        // ladybug_dump($request->request->get('mrw_sitebundle_header'));
        // die();


        

        
        $oTrackingId = $em->getRepository('MRWSiteBundle:TrackingId')->findOneBy(array('trackingId' => $oRequest->request->get('mrw_sitebundle_header')['trackingId']));

// $editForm->isValid() && 

        if ($editForm->isValid() && $oTrackingId instanceof TrackingId) 
        {
            // ladybug_dump($oHeader);
            // die();
            //ici
            $oTrackingId->setUsed(true);
            $oHeader->setTrackingId($oTrackingId);

            $em->persist($oHeader);
            $em->persist($oTrackingId);
            $em->flush();


            return $this->redirect($this->generateUrl('header'));
        }
        else
        {
            
            return $this->redirect($this->generateUrl('header_edit', array('id' => $id)));
        }

        return $this->render('MRWSiteBundle:Header:edit.html.twig', array(
            'entity'      => $oHeader,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Header entity.
     *
     */
    public function deleteAction(Request $oRequest, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($oRequest);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MRWSiteBundle:Header')->findOneBy(array('id' => $id, 'actif' => true));

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Header entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('header'));
    }

    /**
     * Creates a form to delete a Header entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('header_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function readHeaderAction()
    {
        $bFlag = false;
        $session = $this->get('session');

        $finder =  $this->get('MRWFormatParser')->getFilestoRead();

        foreach ($finder as $file) 
        {

            $aFileHeader = $this->get('MRWFormatParser')->read($file->getRelativePathname());

            if (count($aFileHeader))
            {
                $em = $this->getDoctrine()->getManager();
                foreach ($aFileHeader as $iKey => $aHeader) 
                {
                    if(strlen($aHeader['trackingId']) > 0)
                    {
                        $oHeader = $em->getRepository('MRWSiteBundle:Header')->getHeaderByTrackingId($aHeader['trackingId']);

                        $subStatus = $em->getRepository('MRWSiteBundle:SubStatus')->findOneBy(array('subStatus' => $aHeader['substatus'] ));

                        if($oHeader != null && $subStatus != null)
                        {
                            $oHeader->setSubStatus($aHeader['substatus']);
                            $oHeader->setSubStatusText($subStatus->getAction());
                            $em->persist($oHeader);
                            $em->flush();
                            $bFlag = true;                        
                        }

                    }

                }
                
            }
        }

        if ($bFlag == true)
        {
            $session->getFlashBag()->add('noticeFilesRead', 'Fichers lus avec succès !');
        }


        return $this->redirect($this->generateUrl('header'));

    }
}
