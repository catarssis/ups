<?php

namespace MRW\SiteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

// use MRW\SiteBundle\Entity\CodeBarre;
use MRW\SiteBundle\Entity\TrackingId;
use MRW\SiteBundle\Form\TrackingIdType;

/**
 * CodeBarre controller.
 *
 */
class CodeBarreController extends Controller
{

    /**
     * Lists all CodeBarre entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MRWSiteBundle:CodeBarre')->findAll();

        return $this->render('MRWSiteBundle:CodeBarre:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new CodeBarre entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new CodeBarre();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('codebarre_show', array('id' => $entity->getId())));
        }

        return $this->render('MRWSiteBundle:CodeBarre:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a CodeBarre entity.
     *
     * @param CodeBarre $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CodeBarre $entity)
    {
        $form = $this->createForm(new CodeBarreType(), $entity, array(
            'action' => $this->generateUrl('codebarre_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new CodeBarre entity.
     *
     */
    public function newAction()
    {
        $entity = new CodeBarre();
        $form   = $this->createCreateForm($entity);

        return $this->render('MRWSiteBundle:CodeBarre:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a CodeBarre entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MRWSiteBundle:CodeBarre')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CodeBarre entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MRWSiteBundle:CodeBarre:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing CodeBarre entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MRWSiteBundle:CodeBarre')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CodeBarre entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MRWSiteBundle:CodeBarre:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a CodeBarre entity.
    *
    * @param CodeBarre $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(CodeBarre $entity)
    {
        $form = $this->createForm(new CodeBarreType(), $entity, array(
            'action' => $this->generateUrl('codebarre_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing CodeBarre entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MRWSiteBundle:CodeBarre')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CodeBarre entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('codebarre_edit', array('id' => $id)));
        }

        return $this->render('MRWSiteBundle:CodeBarre:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a CodeBarre entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MRWSiteBundle:CodeBarre')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find CodeBarre entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('codebarre'));
    }

    /**
     * Creates a form to delete a CodeBarre entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('codebarre_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    public function findAction(Request $oRequest)
    {
        $oManager = $this->getDoctrine()->getManager();
        $oRepository = $oManager->getRepository('MRWSiteBundle:TrackingId');
        $oTrackingId = $oRepository->findOneBy(array('trackingId' => trim($oRequest->get('trackingId')), 'used' => false));

        //return new JsonResponse(array('success' => $oRequest->get('trackingId')));

        if ( $oTrackingId instanceof TrackingId ) return new JsonResponse(array('success' => true));
        else return new JsonResponse(array('success' => false));
    }
}
