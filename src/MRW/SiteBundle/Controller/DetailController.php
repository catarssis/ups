<?php

namespace MRW\SiteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MRW\SiteBundle\Entity\Detail;
use MRW\SiteBundle\Form\DetailType;

/**
 * Detail controller.
 *
 */
class DetailController extends Controller
{

    /**
     * Lists all Detail entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MRWSiteBundle:Detail')->findBy(array('actif' => true));

        return $this->render('MRWSiteBundle:Detail:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Detail entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Detail();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('detail_show', array('id' => $entity->getId())));
        }

        return $this->render('MRWSiteBundle:Detail:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Detail entity.
     *
     * @param Detail $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Detail $entity)
    {
        $form = $this->createForm(new DetailType(), $entity, array(
            'action' => $this->generateUrl('detail_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Detail entity.
     *
     */
    public function newAction()
    {
        $entity = new Detail();

        // $oHeader = $em->getRepository('MRWSiteBundle:Header')->findOneBy(array('id' => $header_id));
        // $entity->setHeader($oHeader);
        $form   = $this->createCreateForm($entity);

        return $this->render('MRWSiteBundle:Detail:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));


        $entity = new Header();
        $form   = $this->createCreateForm($entity);

    }

    /**
     * Finds and displays a Detail entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MRWSiteBundle:Detail')->findOneBy(array('id' => $id, 'actif' => true));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Detail entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MRWSiteBundle:Detail:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Detail entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MRWSiteBundle:Detail')->findOneBy(array('id' => $id, 'actif' => true));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Detail entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MRWSiteBundle:Detail:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Detail entity.
    *
    * @param Detail $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Detail $entity)
    {
        $form = $this->createForm(new DetailType(), $entity, array(
            'action' => $this->generateUrl('detail_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Detail entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MRWSiteBundle:Detail')->findOneBy(array('id' => $id, 'actif' => true));

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Detail entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('detail_edit', array('id' => $id)));
        }

        return $this->render('MRWSiteBundle:Detail:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Detail entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MRWSiteBundle:Detail')->findOneBy(array('id' => $id, 'actif' => true));

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Detail entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('detail'));
    }

    /**
     * Creates a form to delete a Detail entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('detail_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
