jQuery(document).ready(function(){
    jQuery(".hide-associations").slideUp();
    jQuery(document).on('click', '.preventDefault', function(event) {
        event.preventDefault();
    });
    jQuery(document).on('click', '.show-associations', function(event) {
        event.preventDefault();
        jQuery(this).parents("td").find(".association-list").slideToggle(function(){
            if ( jQuery(".association-list").is(":hidden") )
                jQuery(".show-associations").find("span").text(" (show)");
            else
                jQuery(".show-associations").find("span").text(" (hide)");
        });
    });

    // Activation / désactivation des blocks par langue
    if ( jQuery("select.lang-switcher").length)
    {
        jQuery.each($("select.lang-switcher"), function(index, value){
            jQuery(this).parents("tr").find(".form-group").not("[id*=contenu-" + jQuery(this).val() + "]").css({ display:"none" });
            jQuery(this).parents("tr").find(".form-group[id*=contenu-" + jQuery(this).val() + "]").css({ display:"block" });
        });

        jQuery(document).on("change", "select.lang-switcher", function(){
            jQuery(this).parents("tr").find(".form-group").not("[id*=contenu-" + jQuery(this).val() + "]").css({ display:"none" });
            jQuery(this).parents("tr").find(".form-group[id*=contenu-" + jQuery(this).val() + "]").css({ display:"block" });
        }); 
    }

    jQuery(document).on('click', '.text-accordion-admin', function(){
        // Hide
        if ( jQuery(this).find('i.fa').hasClass('fa-minus-circle') )
        {
            jQuery(this).parents('tbody.title-accordion-admin').next('tbody.content-accordion-admin').hide(500);
            jQuery(this).find('i.fa').removeClass('fa-minus-circle').addClass('fa-plus-circle');
        }
        // Show
        else
        {
            jQuery(this).parents('tbody.title-accordion-admin').next('tbody.content-accordion-admin').show(500);
            jQuery(this).find('i.fa').removeClass('fa-plus-circle').addClass('fa-minus-circle');
        }
    }); 
    jQuery('tbody.content-accordion-admin').hide(500);

    
});